import math
from matrix import Matrix
from nonlineareq import Bisection, NewtonRaphson, Secant
from lineareq import GaussPALU, CholeskyDecomp, GaussSeidel

### Exercise 1
def f1(x):
    exa = math.exp(x-2)
    x3 = x**3
    x2 = x**2

    return (14*x*exa)-(12*exa)-(7*x3)+(20*x2)-(26*x)+12

# first derivative of f1
def f1_der1(x):
    exa = math.exp(x-2)
    x2 = x**2
    
    return (14*x*exa)+(2*exa)-(21*x2)+(40*x)-26

# second derivative of f1
def f1_der2(x):
    exa = math.exp(x-2)
    
    return (14*x*exa)+(16*exa)-(42*x)+40

## 1.1 Bisection
def ex1_1():
    tol = 0.5e-6

    bis = Bisection(f1, 0.0, 1.5, tol)
    root1 = bis.execute()
    bis.set_interval(1.5, 3.0)
    root2 = bis.execute()

    print "-" * 60
    print "RESULTS FOR 1.1 Bisection"
    print "-" * 60
    print "root1: %.8f   interval: [%.2f-%.2f]   loops: %d " % (root1[0], 0.0, 1.5, root1[1])
    print "root2: %.8f   interval: [%.2f-%.2f]   loops: %d " % (root2[0], 1.5, 3.0, root2[1])

## 1.2 Newton-Raphson
def ex1_2():
    tol = 0.5e-6

    nr = NewtonRaphson(f1, f1_der1, f1_der2, 0.0, tol)
    root1 = nr.execute()
    nr.set_x0(3.0)
    root2 = nr.execute()

    print "-" * 60
    print "RESULTS FOR 1.2 Newton-Raphson"
    print "-" * 60
    print "root1: %.8f   x0: %.2f   loops: %d " % (root1[0], 0.0, root1[1])
    print "root2: %.8f   x0: %.2f   loops: %d " % (root2[0], 3.0, root2[1])

# nr = NewtonRaphson(f1, f1_der1, f1_der2, 1.0, 0.5e-6)
# nr.display = DEBUG
# nr.use_alternative(False)
# print nr.execute()

## 1.3 Secant
def ex1_3():
    tol = 0.5e-6

    inp1 = (0.5, 1.1)
    inp2 = (1.7, 2.3)
    
    sec = Secant(f1, tol)
    sec.use_default(inp1[0], inp1[1])
    root1 = sec.execute()

    sec.use_default(inp2[0], inp2[1])
    root2 = sec.execute()

    print "-" * 60
    print "RESULTS FOR 1.3 Secant"
    print "-" * 60
    print "root1: %.8f   x0: %.2f x1: %.2f   loops: %d " % (root1[0], inp1[0], inp1[1], root1[1])
    print "root2: %.8f   x0: %.2f x1: %.2f   loops: %d " % (root2[0], inp2[0], inp2[1], root2[1])

### Exercise 2
def f2(x):
    cosx = math.cos(x)
    sinx = math.sin(x)
    cosx3 = cosx**3
    sinx2 = sinx**2
    sinx4 = sinx**4

    return (94*cosx3)-(24*cosx)+(177*sinx2)-(108*sinx4)-(72*cosx3*sinx2)-65

# first derivative of f2
def f2_der1(x):
    cosx = math.cos(x)
    sinx = math.sin(x)
    cosx2 = cosx**2
    cosx3 = cosx**3
    cosx4 = cosx**4
    sinx2 = sinx**2
    sinx3 = sinx**3
    sinx4 = sinx**4
    
    return 24*sinx + 354*cosx*sinx - 282*cosx2*sinx - 432*cosx*sinx3 - 144*cosx4*sinx + 216*cosx2*sinx3

# second derivative of f2
def f2_der2(x):
    cosx = math.cos(x)
    sinx = math.sin(x)
    cosx2 = cosx**2
    cosx3 = cosx**3
    cosx4 = cosx**4
    cosx5 = cosx**5
    sinx2 = sinx**2
    sinx3 = sinx**3
    sinx4 = sinx**4

    return 24*cosx + 354*cosx2 - 282*cosx3 - 144*cosx5 - 354*sinx2 + 432*sinx4 + 564*cosx*sinx2 - 432*cosx*sinx4 - 1296*cosx2*sinx2 + 1224*cosx3*sinx2

## 2.1 rand Bisection
def ex2_1():
    tol = 0.5e-6
    
    inp = [(0.5, 0.95), (0.95,1.5), (2, 2.5)]
    roots = []

    bis = Bisection(f2, inp[0][0], inp[0][1], tol)
    bis.use_random(True)

    roots.append(bis.execute())
    bis.set_interval(inp[1][0], inp[1][1])
    roots.append(bis.execute())
    bis.set_interval(inp[2][0], inp[2][1])
    roots.append(bis.execute())

    print "-" * 60
    print "RESULTS FOR 2.1 rand-Bisection"
    print "-" * 60
    for i in xrange(3):
        print "root%d: %.8f   interval: [%.2f-%.2f]   loops: %d " % (i+1, roots[i][0], inp[i][0], inp[i][1], roots[i][1])
    

# 2.1.1 rand Bisection x10
def ex2_11():
    res = []
    tol = 0.5e-6
    a = 0.5
    b = 0.95

    bis = Bisection(f2, a, b, tol)
    bis.use_random(True)

    for i in xrange(10):
        bis.set_interval(a, b) # reset
        res.append(bis.execute())

    print "-" * 60
    print "RESULTS FOR 2.1.1 rand-Bisection x10"
    print "-" * 60
    for i in xrange(10):
        print "%2d: root: %.8f    loops: %d" % (i+1, res[i][0], res[i][1])

    # loops
    # for i in xrange(10):
    #     print "& %d " % (res[i][1])

## 2.2 alt Newton-Raphson
def ex2_2():
    roots = []
    inp = [0.3, 1.5, 2.8]
    tol = 0.5e-6

    nr = NewtonRaphson(f2, f2_der1, f2_der2, 0.0, tol)
    nr.use_alternative(True)

    for j in xrange(3):
        nr.set_x0(inp[j])
        roots.append(nr.execute())

    print "-" * 60
    print "RESULTS FOR 2.2 alt-Newton-Raphson"
    print "-" * 60
    for i in xrange(3):
        print "root%d: %.8f   x0: %.2f   loops: %d" % (i+1, roots[i][0], inp[i], roots[i][1])
    

## 2.3 alt Secant
def ex2_3():
    inp = [(0.0, 0.2, 0.4), (0.9, 0.95, 1.0), (2.0, 2.5, 2.8)]
    roots = []
    tol = 0.5e-6
    sec = Secant(f2, tol)

    for j in xrange(3):
        sec.use_alternative(inp[j][0], inp[j][1], inp[j][2])
        roots.append(sec.execute())

    print "-" * 60
    print "RESULTS FOR 2.3 alt-Secant"
    print "-" * 60
    for i in xrange(3):
        print "root%d: %.8f   x0: %.2f x1: %.2f x2: %.2f   loops: %d " % (i+1, roots[i][0], inp[i][0], inp[i][1], inp[i][2], roots[i][1])
    
### Exercise 3

## 3.1
def ex3_1():
    A = [
        [4.0,  2.0, -1.0,  3.0],
        [3.0, -4.0,  2.0,  5.0],
        [-2.0, 6.0, -5.0, -2.0],
        [5.0,  1.0,  6.0, -3.0]
    ]

    b = [16.9, -14, 25.0, 9.4]

    dec = GaussPALU.get_palu_decomposition(A)
    gp = GaussPALU(A, b)
    x = gp.execute()

    print "-" * 60
    print "RESULTS FOR 3.1 Gauss-PALU"
    print "-" * 60
    print "Matrix A:"
    print Matrix(A)
    print "Matrix P:"
    print dec[0]
    print "Matrix L:"
    print dec[1]
    print "Matrix U:"
    print dec[2]
    print "Vector b:"
    print b
    print "Ax=b solution:"
    print x

## 3.2
def ex3_2():
    A = [
        [25.0, 15.0, -5.0],
        [15.0, 18.0, 0.0],
        [-5.0, 0.0, 11.0]
    ]

    ch = CholeskyDecomp(A)
    L = ch.execute()

    print "-" * 60
    print "RESULTS FOR 3.2 CholeskyDecomp"
    print "-" * 60
    print "Matrix A:"
    print Matrix(A)
    print "Matrix L:"
    print L

## 3.3
def ex3_3():
    res = []
    for n in [10, 10000]:
        b = GaussSeidel.create_3c_b(n)
        x0 = [0 for i in xrange(n)]

        # doesn't need to pass an A matrix for the optimized
        # for 3c version because it uses a predefined one to
        # save space and time
        gs = GaussSeidel([[]], b, x0, 0.5e-4)
        gs.set_optimized_3c(True, n)
        gs.display = False
        res.append(gs.execute())

    print "-" * 60
    print "RESULTS FOR 3.3 Gauss Seidel"
    print "-" * 60

    for r in res:
        x = r[0]
        loops = r[1]
        n = len(x)
        print "n: %5d   loops: %d" % (n, loops)
        if n<=10:
            print x
        else:
            print "%s......%s" % (x[0:4], x[n-4:n])
        print "*" * 10
            

### Execution

# Exercise 1
ex1_1()
ex1_2()
ex1_3()

# Exercise 2
ex2_1()
ex2_11()
ex2_2()
ex2_3()

# Exercise 3
ex3_1()
ex3_2()
ex3_3()