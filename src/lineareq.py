import math
import time
from matrix import Matrix

class GaussPALU:
    def __init__(self, A, b):
        self.A = Matrix(A)
        self.b = b

    @staticmethod
    def get_palu_decomposition(A):
        U = Matrix(A) # clone
        n = U.get_size()[0]
        P = Matrix.create_identity(n)
        L = Matrix.create_zero(n)

        for c in xrange(n): # columns
            max_num, max_i = U.get_column_abs_max(c, c)

            if max_num == 0:
                continue

            # swap max row
            if max_i != c:
                U.swap_rows(c, max_i)
                P.swap_rows(c, max_i)
                L.swap_rows(c, max_i)
            
            if c < n-1:
                pivot = U[c][c]
                for r in xrange(c+1, n):
                    cell = U[r][c]
                    if cell == 0:
                        continue

                    p_row = U[c][:]
                    sig = -1.0 if cell*pivot > 0 else 1.0
                    L[r][c] = -sig*(math.fabs(cell)/math.fabs(pivot))
                    for j in xrange(n):
                        p_row[j] = sig*((p_row[j]/math.fabs(pivot))*math.fabs(cell))

                    U.add_to_row(r, p_row)
        
        # fill L diag with ones
        for i in xrange(n):
            L[i][i] = 1

        return (P, L, U)

    def solve_trivial_system(self, M, v, rev=False):
        x = [0 for i in xrange(len(v))]
        rows, columns = M.get_size()
        rrange = reversed(xrange(rows)) if rev else xrange(rows)
        for r in rrange:
            rsum = 0
            yrange = reversed(xrange(r, columns)) if rev else xrange(r+1)
            for c in yrange:
                if r == c:
                    x[r] = (v[r]-rsum)/M[r][r]
                else:
                    rsum += x[c]*M[r][c]
        return x

    def execute(self):
        P, L, U = GaussPALU.get_palu_decomposition(self.A)
        z = P.multiply_with_vector(self.b)
        y = self.solve_trivial_system(L, z)
        return self.solve_trivial_system(U, y, True)

def vector_infinity_norm(vec):
    vec_max = max(vec)
    vec_min = min(vec)

    return max(math.fabs(vec_max), math.fabs(vec_min))

class CholeskyDecomp:
    def __init__(self, A):
        self.A = Matrix(A)
    
    def calculate_diag_value(self, i, L):
        suml = 0
        if i>0:
            suml = sum([L[i][k]**2 for k in xrange(i)])

        return math.sqrt(self.A[i][i] - suml)

    def calculate_cell_value(self, i, j, L):
        suml = 0
        if j>0:
            suml = sum([L[i][k]*L[j][k] for k in xrange(j)])

        return 1.0/L[j][j]*(self.A[i][j] - suml)

    def execute(self):
        size = self.A.get_size()[0]
        L = Matrix.create_zero(size)

        for r in xrange(size): # rows
            for c in xrange(r+1): # columns 
                if r==c:
                    L[r][c] = self.calculate_diag_value(r, L)
                else:
                    L[r][c] = self.calculate_cell_value(r, c, L)
        
        return L

class GaussSeidel:
    optimized_3c = False

    @staticmethod
    def create_3c_A(n):
        return [[5.0 if i==j else -2.0 if i==j+1 or j==i+1 else 0.0 for j in xrange(n)] for i in xrange(n)]
    
    @staticmethod
    def create_3c_b(n):
        return [3.0 if i==0 or i==n-1 else 1.0 for i in xrange(n)]

    def __init__(self, A, b, x0, tol):
        self.A = Matrix(A)
        self.b = b
        self.x0 = x0
        self.tol = tol

    def set_optimized_3c(self, choice, n=None):
        self.optimized_3c = choice
        self.n_3c = n

    def calculate_row_sums(self, i, x):
        A = self.A
        size = self.A.get_size()[0]
        bef_sum = 0
        aft_sum = 0

        if i > 0:
            mult = [A[i][c]*x[c] for c in xrange(i)] # the x[c] contains the updated value
            bef_sum = sum(mult)
        
        if i < size-1:
            mult = [A[i][c]*x[c] for c in xrange(i+1, size)]
            aft_sum = sum(mult)

        return (bef_sum, aft_sum)

    def calculate_row_sums_optimized_3c(self, i, x):
        size = self.n_3c
        bef_sum = 0
        aft_sum = 0

        if i > 0:
            bef_sum = (-2)*x[i-1]
        
        if i < size-1:
            aft_sum = (-2)*x[i+1]

        return (bef_sum, aft_sum)

    def execute(self):
        if self.optimized_3c:
            size = self.n_3c
        else:
            A = self.A
            size = self.A.get_size()[0]
        
        x = self.x0[:]
        prev_norm = None
        loops = 0

        while True:
            loops += 1

            for i in xrange(size):
                if self.optimized_3c:
                    bef_sum, aft_sum = self.calculate_row_sums_optimized_3c(i, x)
                    x[i] = (1.0/5.0)*(self.b[i] - bef_sum - aft_sum)
                else:
                    bef_sum, aft_sum = self.calculate_row_sums(i, x)
                    x[i] = (1.0/A[i][i])*(self.b[i] - bef_sum - aft_sum)
            
            # compare the infinity norms to check
            # if solution has been found
            norm = vector_infinity_norm(x)
            if loops > 1 and math.fabs(norm - prev_norm) < self.tol:
                return (x, loops)
                
            prev_norm = norm
