import math

# Represents a 2dMatrix
class Matrix:
    # creates an n-size identity matrix
    @staticmethod
    def create_identity(n):
        matrix = [[1 if i==j else 0 for j in xrange(n)] for i in xrange(n)]
        return Matrix(matrix)

    # creates an n-size zero matrix
    @staticmethod
    def create_zero(n):
        matrix = [[0 for j in xrange(n)] for i in xrange(n)]
        return Matrix(matrix)
    
    # creates a matrix from a vector
    @staticmethod
    def create_from_vector(vec):
        matrix = [[vec[i]] for i in xrange(len(vec))]
        return Matrix(matrix)

    # constructor
    def __init__(self, matrix):
        if isinstance(matrix, Matrix):
            self.__matrix = matrix.get_matrix()
        else:
            self.__matrix = [l[:] for l in matrix] # clone matrix

        self.__size = (len(self.__matrix), len(self.__matrix[0]))

    # override [] operator
    def __getitem__(self, key):
        return self.__matrix[key]

    # to string method
    def __str__(self):
        # return "%s" % (self.get_matrix())
        matstr = ""
        rows, columns = self.__size
        for r in xrange(rows):
            rstr = "|"
            for c in xrange(columns):
                rstr += " %6.2f |" % (self.__matrix[r][c])
            matstr += rstr + "\n"
        return matstr

    # returns the c-th column of the matrix
    def get_column(self, c):
        col = []
        for i in xrange(self.__size[0]):
            col.append(self.__matrix[i][c])

        return col
    
    # returns the absolute max element of c-th  
    # column of the matrix and its index
    def get_column_abs_max(self, c, start = 0):
        col = self.get_column(c)
        sel = col[start:self.__size[0]]

        max_el = (sel[0], start)
        for i in xrange(len(sel)):
            if math.fabs(sel[i]) > math.fabs(max_el[0]):
                max_el = (sel[i], start + i)
        
        return max_el

    # swaps r1 row with r2
    def swap_rows(self, r1, r2):
        n = self.__size[1]
        tmp_r1 = self.__matrix[r1][:] # clone row

        for i in xrange(n):
            self.__matrix[r1][i] = self.__matrix[r2][i]

        for j in xrange(n):
            self.__matrix[r2][j] = tmp_r1[j]

    def add_to_row(self, r, row):
        for c in xrange(self.__size[1]):
            self.__matrix[r][c] = self.__matrix[r][c] + row[c]

    def multiply_with_vector(self, vec):
        res = []

        for r in xrange(self.__size[0]):
            elsum = 0
            for c in xrange(self.__size[1]):
                elsum += self.__matrix[r][c]*vec[c]
            res.append(elsum)

        return res

    # returns the infinity norm of the matrix
    def infinity_norm(self):
        rows = [0 for i in xrange(self.__size[0])]
        maxr = 0

        # calculate the abs sum of every row
        for r in xrange(self.__size[0]):
            for c in xrange(self.__size[1]):
                rows[r] += math.fabs(self.__matrix[r][c])
            
            # check if row r sum is bigger than previous max
            if rows[r] > maxr:
                maxr = rows[r]
        
        return maxr

    # returns a copy of the raw matrix
    def get_matrix(self):
        return [l[:] for l in self.__matrix]
    
    # returns the size of the matrix
    def get_size(self):
        return self.__size
