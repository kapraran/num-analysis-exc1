import random
import math

class Bisection:
    rand = False

    def __init__(self, f, a, b, tol):
        self.f = f
        self.interval = (a, b)
        self.tol = tol

    def set_interval(self, a, b):
        self.interval = (a, b)

    def use_random(self, choice):
        self.rand = choice

    def choose_m(self, a, b):
        if self.rand:
            return a + random.random()*(b-a)
        return (a+b)/2.0

    def max_error(self, a, b):
        if self.rand:
            return b-a
        else:
            return (b-a)/2.0

    def execute(self):
        a, b = self.interval
        f = self.f

        if f(a)*f(b) >= 0:
            raise Exception("Invalid interval selected")
        
        loops = 0
        while True:
            loops += 1
            # new midpoint
            m = self.choose_m(a, b)

            va = f(a)
            vb = f(b)
            vm = f(m)

            # check if m is root or max error is less than tolerance
            if vm == 0.0 or self.max_error(a, b) < self.tol:
                return (m, loops)

            if va*vm < 0:
                b = m
            else:
                a = m

class NewtonRaphson:
    alt = False

    def __init__(self, f, f_d1, f_d2, x0, tol):
        self.f = f
        self.f_d1 = f_d1
        self.f_d2 = f_d2
        self.x0 = x0
        self.tol = tol
    
    def set_x0(self, x0):
        self.x0 = x0

    def use_alternative(self, choice):
        self.alt = choice

    def calculate_next(self, fp, fp_d1, prev):
        if self.alt:
            fp_d2 = self.f_d2(prev)
            return prev - fp/fp_d1 - 0.5*(fp**2*fp_d2)/(fp_d1**3)
        else:
            return prev - fp/fp_d1

    def execute(self):
        prev = self.x0

        loops = 0
        while True:
            loops += 1
            fp = self.f(prev)
            fp_d1 = self.f_d1(prev)

            if fp_d1 == 0:
                return (prev, loops-1, "error: zero devision")

            nxt = self.calculate_next(fp, fp_d1, prev)

            if self.f(nxt) == 0 or math.fabs(nxt - prev) < self.tol:
                return (nxt, loops)

            prev = nxt

class Secant:
    alt = False

    def __init__(self, f, tol):
        self.f = f
        self.tol = tol

    def use_default(self, x0, x1):
        self.alt = False
        self.x0 = x0
        self.x1 = x1

    def use_alternative(self, x0, x1, x2):
        self.alt = True
        self.alt_prev = [x0, x1, x2]

    def execute_alternative(self):
        prev = self.alt_prev
        f = self.f
        loops = 0

        while True:
            loops += 1

            # newer value
            newer = prev[(loops-2) % 3]

            q = f(prev[0])/f(prev[1])
            r = f(prev[2])/f(prev[1])
            s = f(prev[2])/f(prev[0])

            num = r*(r-q)*(prev[2]-prev[1])+(1-r)*s*(prev[2]-prev[0])
            den = (q-1)*(r-1)*(s-1)

            if den == 0:
                return (newer, loops, "error: zero division")

            nxt = prev[2] - num/den
            if self.f(nxt) == 0 or math.fabs(nxt - newer) < self.tol:
                return (nxt, loops)
            
            # replace oldest value
            prev[(loops-1) % 3] = nxt


    def execute(self):
        if self.alt:
            return self.execute_alternative()
        
        prev0 = self.x0
        prev1 = self.x1
        loops = 0

        while True:
            loops += 1
            fp0 = self.f(prev0)
            fp1 = self.f(prev1)

            if fp1 - fp0 == 0:
                return (nxt, loops, "error: zero division")

            nxt = prev1 - (fp1*(prev1 - prev0))/(fp1 - fp0)

            if self.f(nxt) == 0 or math.fabs(nxt - prev1) < self.tol:
                return (nxt, loops)

            prev0 = prev1
            prev1 = nxt